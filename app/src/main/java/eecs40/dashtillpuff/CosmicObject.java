package eecs40.dashtillpuff;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.SurfaceView;

import java.util.Random;

/**
 * Created by Ahmed on 4/16/2015.
 */
public class CosmicObject implements TimeConscious {
	private static final int RADIUS_INC    = 5;
	private static final int RADIUS_DEC    = 5;
	public  static final int RADIUS_MAX    = 100;
	public  static final int RADIUS_MIN    = 50;
	public  static final int VELOCITY_MIN  = 5;
	public  static final int VELOCITY_MAX  = 10;
	private static final int DEVIATION_MAX = 20;
	private static final int ALPHA_DEC     = 20;

	private int velocityX;
	private int velocityY;
	private int centerX;
	private int centerY;
	private int dockX;
	private int dockY;
	private int radius;
	private int alpha;
	private boolean destroyed;
	private final Bitmap bitmap;
	private final DashTillPuffSurfaceView sv;
	private static Bitmap[] bitmaps;
	private static Random rng = new Random();

	public CosmicObject(DashTillPuffSurfaceView sv, Bitmap bitmap, int centerX, int centerY, int radius) {
		this.sv        = sv;
		this.dockX     = centerX;
		this.dockY     = centerY;
		this.centerX   = centerX;
		this.centerY   = centerY;
		this.radius    = radius;
		this.bitmap    = bitmap;
		this.alpha     = 255;
		this.destroyed = false;
		this.velocityX = VELOCITY_MIN + rng.nextInt( VELOCITY_MAX - VELOCITY_MIN );
		this.velocityY = VELOCITY_MIN + rng.nextInt( VELOCITY_MAX - VELOCITY_MIN );
	}

	public int getRadius () { return radius;  }
	public int getCenterX() { return centerX; }
	public int getCenterY() { return centerY; }

	private void tickCenter() {
		if ( DashTillPuffSurfaceView.RIGHT_TO_LEFT ) {
			dockX   += DashTillPuffBackground.OFFSET_X_INC;
			centerX += DashTillPuffBackground.OFFSET_X_INC;
		} else {
			dockX   -= DashTillPuffBackground.OFFSET_X_INC;
			centerX -= DashTillPuffBackground.OFFSET_X_INC;
		}
		centerX += velocityX;
		centerY += velocityY;
		if ( centerX > dockX + DEVIATION_MAX ) {
			velocityX = -velocityX;
			centerX = dockX + DEVIATION_MAX;
		} else if ( centerX < dockX - DEVIATION_MAX ) {
			velocityX = -velocityX;
			centerX = dockX - DEVIATION_MAX;
		}
		if ( centerY > dockY + DEVIATION_MAX ) {
			velocityY = -velocityY;
			centerY = dockY + DEVIATION_MAX;
		} else if ( centerY < dockY - DEVIATION_MAX ) {
			velocityY = -velocityY;
			centerY = dockY - DEVIATION_MAX;
		}
	}

	private boolean isExpanding() {
		int w = sv.getWidth();
		if ( DashTillPuffSurfaceView.RIGHT_TO_LEFT ) {
			return( centerX < (int)(0.33 * w ) );
		} else {
			return( centerX > (int)(0.67 * w ) );
		}
	}

	private boolean isShrinking() {
		int w = sv.getWidth();
		return( ( centerX > (int)(0.33 * w ) ) && ( centerX < (int)(0.67 * w ) ) );
	}

	@Override
	public void tick(Canvas canvas) {
		tickCenter();
		int h = sv.getHeight();
		if ( this.isExpanding() ) {
			radius += RADIUS_INC;
			if (radius > RADIUS_MAX) {
				radius = RADIUS_MAX;
			} else if (radius > (h - centerY)) {
				radius = h - centerY;
			}
		} else if ( this.isShrinking() ) {
			radius -= RADIUS_DEC;
			if ( radius < RADIUS_MIN ) {
				radius = RADIUS_MIN;
			}
		}
		testCollision();
		if ( destroyed ) {
			alpha -= ALPHA_DEC;
			if ( alpha < 0 ) {
				alpha = 0;
			}
		}
		draw(canvas);
	}

	private void testCollision() {
		Bullet bullet = sv.getBullet();
		int dx = this.centerX - bullet.getCenterX();
		int dy = this.centerY - bullet.getCenterY();
		double dist = Math.sqrt( dx*dx + dy*dy );
		if ( dist < this.radius + bullet.getRadius() ) {
			this.destroyed = true;
		}
	}

	public  boolean isDestroyed() { return( alpha == 0 ); }

	private void draw( Canvas c ) {
//			Paint paint = new Paint();
//			paint.setAntiAlias(true);
//			paint.setStyle(Paint.Style.FILL );
//			paint.setColor(Color.YELLOW );
//			c.drawCircle( centerX, centerY, radius, paint );
//			paint.setColor(Color.BLACK );
//			paint.setStyle(Paint.Style.STROKE );
//			c.drawCircle( centerX, centerY, radius, paint );
		Paint paint = new Paint();
		paint.setAlpha( alpha );
		Rect src = new Rect( 0, 0, bitmap.getWidth(), bitmap.getHeight());
		Rect dst = new Rect( centerX-radius, centerY-radius,
				centerX+radius, centerY+radius);
		c.drawBitmap(bitmap, null, dst, paint);
	}


	public static Bitmap getRandomBitmap() {
		return bitmaps[ rng.nextInt( bitmaps.length ) ];
	}

	public static void loadBitMaps( SurfaceView sv ) {
		if ( bitmaps != null ) return;
		BitmapFactory.Options options = new BitmapFactory.Options();
		bitmaps = new Bitmap[9];
		bitmaps[0] = BitmapFactory.decodeResource( sv.getResources(),
				R.drawable.stock_draw_sphere, options );
		bitmaps[1] = BitmapFactory.decodeResource( sv.getResources(),
				R.drawable.bookmark, options );
		bitmaps[2] = BitmapFactory.decodeResource( sv.getResources(),
				R.drawable.bookmark_1, options );
		bitmaps[3] = BitmapFactory.decodeResource( sv.getResources(),
				R.drawable.cloud, options );
		bitmaps[4] = BitmapFactory.decodeResource( sv.getResources(),
				R.drawable.golden_star, options );
		bitmaps[5] = BitmapFactory.decodeResource( sv.getResources(),
				R.drawable.golf_ball, options );
		bitmaps[6] = BitmapFactory.decodeResource( sv.getResources(),
				R.drawable.magic_ball, options );
		bitmaps[7] = BitmapFactory.decodeResource( sv.getResources(),
				R.drawable.star, options );
		bitmaps[8] = BitmapFactory.decodeResource( sv.getResources(),
				R.drawable.keditbookmarks, options );
	}



}



