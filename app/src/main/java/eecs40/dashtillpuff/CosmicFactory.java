package eecs40.dashtillpuff;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Ahmed on 4/15/2015.
 */
public class CosmicFactory implements TimeConscious {


	private ArrayList<CosmicObject> cosmicObjects;
	private final DashTillPuffSurfaceView sv;
	private final Trajectory  trajectory;
	private int lastX;
	private int curClusterCount;
	private boolean upperSide;

	private static final int MARGIN_Y  = 100;
	private static final int CLUSTER_X = 150;
	private static final int CLUSTER_Y = 150;
	private static final int NUM_BALLS = 10;

	public CosmicFactory(DashTillPuffSurfaceView sv, Trajectory trajectory) {
		this.sv = sv;
		this.trajectory = trajectory;
		this.upperSide  = false;
		this.cosmicObjects = new ArrayList<CosmicObject>();
		this.curClusterCount = 0;
		boolean r2l = DashTillPuffSurfaceView.RIGHT_TO_LEFT;
		int w = sv.getWidth();
		if ( r2l ) { this.lastX = 0; }
		else       { this.lastX = w; }
	}

	private Bitmap curClusterBitmap;

	public void genCluster() {
		int r = ( CosmicObject.RADIUS_MIN + CosmicObject.RADIUS_MAX )/2;
		int h = sv.getHeight();
		int w = sv.getWidth();

		if ( curClusterBitmap == null ) {
			CosmicObject.loadBitMaps(sv);
			curClusterBitmap = CosmicObject.getRandomBitmap();
		}
		boolean r2l = DashTillPuffSurfaceView.RIGHT_TO_LEFT;
		while( curClusterCount < NUM_BALLS ) {
			if ( r2l ) { if( lastX < -w*Trajectory.BUFFER_SIZE   ) break; }
			else       { if( lastX >  w*Trajectory.BUFFER_SIZE+w ) break; }
			int yt = trajectory.getY( lastX );
			if ( yt < 0 ) break;

			int y = (upperSide)? (yt-r-MARGIN_Y) : (yt+r+MARGIN_Y);
			while( true ) {
				if(  upperSide && (y-r < 0) ) break;
				if( !upperSide && (y+r > h) ) break;
				cosmicObjects.add( new CosmicObject( sv, curClusterBitmap, lastX, y, r ) );
				y = (upperSide)? (y-CLUSTER_Y) : (y+CLUSTER_Y);
				if( ++curClusterCount >= NUM_BALLS ) break;
			}
			if ( r2l ) { lastX -= CLUSTER_X; }
			else       { lastX += CLUSTER_X; }
		}
		if ( curClusterCount >= NUM_BALLS ) {
			upperSide = !upperSide;
			curClusterCount = 0;
			curClusterBitmap = null;
		}
	}

	@Override
	public void tick(Canvas canvas) {
		if ( DashTillPuffSurfaceView.RIGHT_TO_LEFT ) {
			lastX += DashTillPuffBackground.OFFSET_X_INC;
		} else {
			lastX -= DashTillPuffBackground.OFFSET_X_INC;
		}
		for( CosmicObject ball : cosmicObjects) {
			ball.tick( canvas );
		}
		reclaimBalls();
		genCluster();
	}


	private void reclaimBalls() {
		int w = sv.getWidth();
		Iterator<CosmicObject> itr = cosmicObjects.iterator();
		while( itr.hasNext() ) {
			CosmicObject cosmicObject = itr.next();
			if ( cosmicObject.isDestroyed() ) {
				itr.remove();
			} else if ( DashTillPuffSurfaceView.RIGHT_TO_LEFT ) {
				if ( cosmicObject.getCenterX() - cosmicObject.getRadius() > w ) {
					itr.remove();
				}
			} else if ( cosmicObject.getCenterX() + cosmicObject.getRadius() < 0 ) {
				itr.remove();
			}
		}
	}

}
