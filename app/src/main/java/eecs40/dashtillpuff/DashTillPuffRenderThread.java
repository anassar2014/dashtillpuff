package eecs40.dashtillpuff;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

/**
 * Created by Ahmed on 4/14/2015.
 */
public class DashTillPuffRenderThread extends Thread {

    private final DashTillPuffSurfaceView view;
    private static final int FRAME_PERIOD = 5; // ms

    public DashTillPuffRenderThread( DashTillPuffSurfaceView view ) {
        this.view = view;
    }

    @Override
    public void run() {
        SurfaceHolder sh = view.getHolder();

        while( !Thread.interrupted() ) {
            Canvas c = sh.lockCanvas(null);
            try {
                synchronized(sh) {
                    view.tick(c);
                }
            } catch (Exception e) {
                System.err.print( e.getStackTrace() );
            } finally {
                if (c != null) {
                    sh.unlockCanvasAndPost(c);
                }
            }
            // Set the frame rate by setting this delay
            try {
                Thread.sleep( FRAME_PERIOD );
            } catch (InterruptedException e) {
                // This means that this thread was interrupted while sleeping.
                return;
            }
        }
    }
}
