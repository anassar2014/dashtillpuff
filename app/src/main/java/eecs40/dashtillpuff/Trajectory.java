package eecs40.dashtillpuff;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

/**
 * Created by Ahmed on 4/15/2015.
 */
public class Trajectory implements  TimeConscious {

	private ArrayList<Point2D> points;
	private final SurfaceView sv;
	private final Random rng;
	private static final int DELTA_X      = 250;
	private static final int DELTA_Y      = 250;
	private static final int ALPHA        = 32;
	private static final int LINE_COLOR   = Color.CYAN;
	private static final int STROKE_WIDTH = 8;
	public  static final int BUFFER_SIZE  = 2;

	public Trajectory( SurfaceView sv ) {
		this.sv     = sv;
		this.rng    = new Random();
		this.points = new ArrayList<Point2D>();
	}

	@Override
	public void tick(Canvas canvas) {
		shiftPoints();
		extendPoints();
		draw(canvas);
	}

	private void draw( Canvas c ) {
		if ( points.isEmpty() ) return;
		int w = sv.getWidth();
		int h = sv.getHeight();

		if ( points.get(0).x > w ) return;
		if ( points.get(0).x < 0 ) return;

		Path path = new Path();
		path.moveTo( points.get(0).x, points.get(0).y );
		for( int i = 1; i < points.size(); ++i ) {
			Point2D p = points.get(i);

			if ( p.x > w ) break;
			if ( p.x < 0 ) break;

			path.lineTo( p.x, p.y );
		}
		Paint paint = new Paint();
		paint.setAlpha( ALPHA );
		paint.setColor( LINE_COLOR );
		paint.setAntiAlias( true );
		paint.setStrokeWidth( STROKE_WIDTH );
		paint.setStrokeJoin( Paint.Join.ROUND );
		paint.setStrokeCap( Paint.Cap.ROUND );
		paint.setStyle( Paint.Style.STROKE );
		paint.setPathEffect(new DashPathEffect(new float[] {10, 20}, 0));
		c.drawPath( path, paint );
	}

	private void extendPoints() {
		int w = sv.getWidth();
		int h = sv.getHeight();
		int x = w;
		if ( !points.isEmpty() ) {
			if ( DashTillPuffSurfaceView.RIGHT_TO_LEFT ) {
				x = points.get( points.size()-1 ).x - DELTA_X;
			} else {
				x = points.get( points.size()-1 ).x + DELTA_X;
			}
		} else if ( DashTillPuffSurfaceView.RIGHT_TO_LEFT ) {
			x = w;
		} else {
			x = 0;
		}
		if ( DashTillPuffSurfaceView.RIGHT_TO_LEFT ) {
			for( ; x >= -BUFFER_SIZE*w; x -= DELTA_X ) {
				points.add( new Point2D( x, getNextY() ) );
			}
		} else {
			for( ; x <= (BUFFER_SIZE+1)*w; x += DELTA_X ) {
				points.add( new Point2D( x, getNextY() ) );
			}
		}
	}

	private int getNextY() {
		int h = sv.getHeight();
		int y = h/2 - DELTA_Y + rng.nextInt(2*DELTA_Y);
		return y;
	}

	private void shiftPoints() {
		int w = sv.getWidth();
		Iterator<Point2D> itr = points.iterator();
		while( itr.hasNext() ) {
			Point2D p = itr.next();
			if ( DashTillPuffSurfaceView.RIGHT_TO_LEFT ) {
				p.x += DashTillPuffBackground.OFFSET_X_INC;
				if ( p.x > w ) {
					itr.remove();
				}
			} else {
				p.x -= DashTillPuffBackground.OFFSET_X_INC;
				if ( p.x < 0 ) {
					itr.remove();
				}
			}
		}
	}

	private Point2D getLowPoint( int x ) {
		Point2D pLow  = null;
		for( Point2D point : points ) {
			if (point.x >= x) continue;
			if ((pLow != null) && (point.x <= pLow.x)) continue;
			pLow = point;
		}
		return pLow;
	}

	private Point2D getHighPoint( int x ) {
		Point2D pHigh  = null;
		for( Point2D point : points ) {
			if (point.x < x) continue;
			if ((pHigh != null) && (point.x >= pHigh.x)) continue;
			pHigh = point;
		}
		return pHigh;
	}

	public int getY( int x ) {
		Point2D pLow  = this.getLowPoint(x);
		Point2D pHigh = this.getHighPoint(x);
		if( pLow  == null ) return -1;
		if( pHigh == null ) return -1;

		float slope = (float)( pHigh.y - pLow.y )/(float)( pHigh.x - pLow.x );
		return( pLow.y + (int)( slope * (x - pLow.x) ) );
	}

}
