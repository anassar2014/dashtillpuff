package eecs40.dashtillpuff;

/**
 * Created by Ahmed on 4/16/2015.
 */

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.SurfaceView;

/**
 * Created by Ahmed on 4/16/2015.
 */
public class Bullet implements TimeConscious {
	public  static final int   THRUST      = 5;
	public  static final int   GRAVITATION = 5;
	public  static final float FRICTION    = 0.2f;
	public  static final int   RADIUS      = 100;

	private       int acceleration;
	private       int velocity;
	private final int centerX;
	private       int centerY;
	private final Bitmap bitmap;
	private final SurfaceView sv;

	public Bullet( SurfaceView sv ) {
		this.sv = sv;
		this.acceleration = 0;
		this.velocity     = 0;
		BitmapFactory.Options options = new BitmapFactory.Options();
		this.bitmap = BitmapFactory.decodeResource( sv.getResources(),
				R.drawable.rocket1, options );
		this.centerY = sv.getWidth()/2;
		if ( DashTillPuffSurfaceView.RIGHT_TO_LEFT ) {
			this.centerX = 2 * sv.getWidth() / 3;
		} else {
			this.centerX = 1 * sv.getWidth() / 3;
		}
	}

	public void setFalling() {
		this.acceleration = GRAVITATION; // Falling
	}

	public void setRising() {
		this.acceleration = -THRUST; // Rising
	}

	private void tickCenter() {
		velocity += acceleration - FRICTION * velocity;
		centerY  += velocity;
		if ( centerY > sv.getHeight() - RADIUS ) {
			centerY  = sv.getHeight() - RADIUS;
			velocity = 0;
		} else if ( centerY < RADIUS ) {
			centerY  = RADIUS;
			velocity = 0;
		}
	}

	public int getCenterX() { return centerX; }
	public int getCenterY() { return centerY; }
	public int getRadius () { return RADIUS;  }

	@Override
	public void tick(Canvas canvas) {
		tickCenter();
		draw(canvas);
	}

	private void draw( Canvas c ) {
		Rect dst = new Rect( centerX-RADIUS, centerY-RADIUS,
				centerX+RADIUS, centerY+RADIUS);
//		c.rotate( 45 );
		c.drawBitmap(bitmap, null, dst, null);
//		c.rotate( -45 );
	}




}



