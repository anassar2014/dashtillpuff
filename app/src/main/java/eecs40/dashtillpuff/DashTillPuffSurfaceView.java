package eecs40.dashtillpuff;

import android.content.Context;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by Ahmed on 4/14/2015.
 */

public class DashTillPuffSurfaceView extends SurfaceView
		implements SurfaceHolder.Callback, TimeConscious {

    private DashTillPuffBackground    background;
    private DashTillPuffRenderThread  renderThread;
	private Trajectory                trajectory;
	private CosmicFactory factory;
	private Bullet                    bullet;

	public static final boolean RIGHT_TO_LEFT = false;

    public DashTillPuffSurfaceView(Context context) {
        super(context);
        // Notify the SurfaceHolder that you'd like to receive SurfaceHolder callbacks.
        getHolder().addCallback(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        renderThread = new DashTillPuffRenderThread(this);
        renderThread.start();
		background = new DashTillPuffBackground(this);
		trajectory = new Trajectory( this );
		factory    = new CosmicFactory(this, trajectory);
		bullet     = new Bullet(this);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        renderGame(canvas);
    }

    protected void renderGame(Canvas c) {
		c.clipRect(0, 0, getWidth(), getHeight());
        background.fillBackground(c);
        background.tick(c);
		trajectory.tick(c);
		factory   .tick(c);
		bullet    .tick(c);
    }

    @Override
    public void tick(Canvas c) {
        renderGame(c);
    }

	@Override
	public boolean onTouchEvent(MotionEvent e) {

		switch ( e.getAction() ) {
			case MotionEvent.ACTION_DOWN:
				bullet.setRising();
				break;
			case MotionEvent.ACTION_UP:
				bullet.setFalling();
				break;
		}
		return true;
	}

	public Bullet getBullet() { return bullet; }
}
